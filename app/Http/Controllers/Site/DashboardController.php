<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\ProductContract;
use App\Contracts\CategoryContract;

class DashboardController extends Controller
{
	protected $productRepository;

	protected $categoryRepository;

	public function __construct(ProductContract $productRepository,
        CategoryContract $categoryRepository
	)
	{
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
	}
    public function index()
    {
		$categories = $this->categoryRepository->findMainCategories();
		
	
		$subcategories = $this->categoryRepository->findSubcategories();
		$products = $this->productRepository->paginateProduct();
		$r_product = $this->productRepository->recommendProduct();
        return view('site.pages.homepage', compact('categories', 'subcategories', 'r_product'));
    }
}
