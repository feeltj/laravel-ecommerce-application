@extends('site.app')
@section('title', 'HomePage')
@section('content')

<div class="card-body">

<div class="row">
	<aside class="col-lg col-md-3 flex-lg-grow-0">
		<h6>MY MARKETS</h6>
		<nav class="nav-home-aside">
    
			<ul class="menu-category">
          @foreach($categories as $category)
        
            <li><a href="{{ route('category.show', $category->slug) }}">{{ $category->name}}</a>
     
					<ul class="submenu">
              
              @foreach($category->children as $sub)
            
            <li><a href="{{ route('category.show', $sub->slug) }}">{{ $sub->name }}</a></li>

            @foreach($sub->children as $s)
            <ul>
                @if(count( $sub->children) > 0 )
                <li><a href="{{ route('category.show', $s->slug) }}">{{ $s->name }}</a></li>
                @endif
            </ul>
             
            @endforeach
          
            @endforeach
         
          </ul>
          @endforeach
        </li>

        
      </ul>
 
		</nav>
	</aside> <!-- col.// -->
	<div class="col-md-9 col-xl-7 col-lg-7">

<!-- ================== COMPONENT SLIDER  BOOTSTRAP  ==================  -->
<div id="carousel1_indicator" class="slider-home-banner carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel1_indicator" data-slide-to="0" class=""></li>
    <li data-target="#carousel1_indicator" data-slide-to="1" class=""></li>
    <li data-target="#carousel1_indicator" data-slide-to="2" class="active"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item">
      <img src="{{ asset('frontend/images/slide1.jpg') }}" alt="First slide"> 
    </div>
    <div class="carousel-item active carousel-item-left">
      <img src="{{ asset('frontend/images/slide2.jpg') }}" alt="Second slide">
    </div>
    <div class="carousel-item carousel-item-next carousel-item-left">
      <img src="{{ asset('frontend/images/slide3.jpg') }}" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carousel1_indicator" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel1_indicator" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> 
<!-- ==================  COMPONENT SLIDER BOOTSTRAP end.// ==================  .// -->	

	</div> <!-- col.// -->
	<div class="col-md d-none d-lg-block flex-grow-1">
		<aside class="special-home-right">
			<h6 class="bg-blue text-center text-white mb-0 p-2">Popular category</h6>
			
			<div class="card-banner border-bottom">
			  <div class="py-3" style="width:80%">
			    <h6 class="card-title">Men clothing</h6>
			    <a href="#" class="btn btn-secondary btn-sm"> Source now </a>
			  </div> 
			  <img src="{{ asset('frontend/images/1.jpg') }}" height="80" class="img-bg">
			</div>

			<div class="card-banner border-bottom">
			  <div class="py-3" style="width:80%">
			    <h6 class="card-title">Winter clothing </h6>
			    <a href="#" class="btn btn-secondary btn-sm"> Source now </a>
			  </div> 
			  <img src="{{ asset('frontend/images/2.jpg') }}" height="80" class="img-bg">
			</div>

			<div class="card-banner border-bottom">
			  <div class="py-3" style="width:80%">
			    <h6 class="card-title">Home inventory</h6>
			    <a href="#" class="btn btn-secondary btn-sm"> Source now </a>
			  </div> 
			  <img src="{{ asset('frontend/images/3.jpg') }}" height="80" class="img-bg">
			</div>

		</aside>
	</div> <!-- col.// -->
</div> <!-- row.// -->

	</div>

<section class="padding-bottom-sm">

        <header class="section-heading heading-line">
            <h4 class="title-section text-uppercase">Recommended items</h4>
        </header>
        <div class="container">
            <div id="code_prod_complex">
                <div class="row row-sm">
                    @forelse($r_product as $product)
                        <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                            <figure class="card card-sm card-product-grid">
                                @if ($product->images->count() > 0)
                                    <div class="img-wrap padding-y"><img src="{{ url('storage/'.$product->images->first()->full) }}" alt=""></div>
                                @else
                                    <div class="img-wrap padding-y"><img src="https://via.placeholder.com/176" alt=""></div>
                                @endif
                                <figcaption class="info-wrap">
                                    <a href="{{ route('product.show', $product->slug) }}" class="title">{{ $product->name }}</a>
                                </figcaption>
                                <form action="{{ route('product.add.cart') }}" method="POST" role="form">
                                    @csrf
                            <input class="form-control" type="number" min="1" value="1" max="{{ $product->quantity }}" name="qty" style="width:70px;margin: 0 auto;">
            <input type="hidden" name="productId" value="{{ $product->id }}">
            <input type="hidden" name="price" value="{{ $product->sale_price != '' ? $product->sale_price : $product->price }}">
                                <button type="submit"  class="btn btn-sm btn-success float-right"><i class="fas fa-shopping-cart"></i></button>
                            <div class="price mt-1">
                                    @if ($product->sale_price != 0)
                                   
                                        <span class="price"> {{ config('settings.currency_symbol').$product->sale_price }} </span>
                                        <del class="price-old"> {{ config('settings.currency_symbol').$product->price }}</del>
                                
                                @else
                                
                                        <span class="price"> {{ config('settings.currency_symbol').$product->price }} </span>
                                
                                @endif
                            </form>
                            </div> <!-- price-wrap.// -->
                            </figure>
                        </div>
                    @empty
                        <p>No Products found in {{ $category->name }}.</p>
                    @endforelse
                </div>
            </div>
      
</section>
@stop
